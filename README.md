# Hackathon Brapi



## Présentation de l'atelier

La Breeding API ([BrAPI](https://brapi.org/)) est un projet visant à améliorer l’interopérabilité des systèmes d’informations en amélioration des plantes : description des ressources génétique, phénotypage, génotypage sont les natures de données au cœur de la BrAPI. Dans cet atelier les participants auront pour objectif d’implémenter des méthodes dans des systèmes d’informations développés à INRAE. Deux scénarios sont possibles, (1) vous développez un SI au sein de votre unité et souhaitez en améliorer l’interopérabilité, vous pouvez proposer votre SI à l’atelier, venir avec votre environnement déjà configuré et travailler sur votre outil, dans ce cas il serait souhaitable que votre code source soit accessible sur une forge et un jeux de données disponible afin que d’autres participants puissent, éventuellement, contribuer à votre projet de développement (2) vous souhaitez participer à l’atelier mais n’avez pas de SI à proposer, aucun souci, vous pouvez contribuer au développement d’un des SI qui sera proposé à l’atelier, ce scénario demandera un travail de configuration d’un environnement en amont de l’atelier afin d’être opérationnel le jour J. En amont de l’atelier un webinaire sera organisé afin de présenter la BrAPI.

## Résultats de l'atelier

- Le hackathon BrAPI a regroupé 9 participants en présentiel et 2 à distance.
- Des méthodes de la BrAPI ont été implémentées dans 5 outils : [Thaliadb](https://sourcesup.renater.fr/projects/thaliadb/), [SHiNeMaS](https://sourcesup.renater.fr/projects/shinemas/), [OLGA](https://forgemia.inra.fr/olga/olga3), [OpenSilex](https://github.com/OpenSILEX/), [GnpIS]()

La réstitution de l'atelier est accéssible [ici](https://forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_4_si_brapi/hackathon-brapi/-/blob/main/restitution_atelier_BrAPI.pptx)
